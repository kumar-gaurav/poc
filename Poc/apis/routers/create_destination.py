from fastapi import FastAPI, HTTPException,APIRouter
import requests
from dotenv import load_dotenv
from . import schema
import os
load_dotenv()


router = APIRouter()
auth_token = os.getenv("AUTH_TOKEN")
workspace_id = os.getenv("WORKSPACE_ID")
create_destination_mongo_url = os.getenv("CREATE_DESTINATION_URL")



@router.post("/create_destination_mongo/")
async def create_destination(destination_data: schema.DestinationCreate):
    url = create_destination_mongo_url
    headers = {
        "accept": "application/json",
        "content-type": "application/json",
        "authorization": f"Bearer {auth_token}"
    }
    payload = {
        "configuration": {
            "destinationType": "mongodb",
            "instance_type": {
                "instance": "atlas",
                "cluster_url": destination_data.cluster_url
            },
            "auth_type": {
                "authorization": "login/password",
                "username": destination_data.username,
                "password": destination_data.password
            },
            "tunnel_method": {"tunnel_method": "NO_TUNNEL"},
            "database": destination_data.database
        },
        "name": destination_data.name,
        "workspaceId": workspace_id
    }

    try:
        response = requests.post(url, json=payload, headers=headers)
        response.raise_for_status() 
        return response.json()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/list_destinations/")
async def list_destinations():
    url = "https://api.airbyte.com/v1/destinations?includeDeleted=false&limit=20&offset=0"
    headers = {
        "accept": "application/json",
        "authorization": f"Bearer {auth_token}"
    }

    response = requests.get(url, headers=headers)

    if response.status_code != 200:
        raise HTTPException(status_code=response.status_code, detail=response.text)

    return response.json()