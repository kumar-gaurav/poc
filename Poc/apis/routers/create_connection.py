from fastapi import FastAPI, HTTPException,APIRouter
import requests
from dotenv import load_dotenv
from . import schema
import os
load_dotenv()


router = APIRouter()
auth_token = os.getenv("AUTH_TOKEN")
workspace_id = os.getenv("WORKSPACE_ID")
connection_url = os.getenv("CONNECTION_URL")

@router.post("/create_connection/")
async def create_connection(connection_data: schema.ConnectionCreate):
    url = connection_url
    headers = {
        "accept": "application/json",
        "content-type": "application/json",
        "authorization": f"Bearer {auth_token}"
    }
    payload = {
        "schedule": {"scheduleType": "manual"},
        "dataResidency": "auto",
        "namespaceDefinition": "destination",
        "namespaceFormat": None,
        "nonBreakingSchemaUpdatesBehavior": "ignore",
        "name": connection_data.name,
        "sourceId": connection_data.source_id,
        "destinationId": connection_data.destination_id
    }

    try:
        response = requests.post(url, json=payload, headers=headers)
        response.raise_for_status()  # Raise an HTTPError for non-2xx status codes
        return response.json()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/list_connections/")
async def list_connections():
    url = connection_url
    headers = {
        "accept": "application/json",
        "authorization": f"Bearer {auth_token}"
    }

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()  # Raise an HTTPError for non-2xx status codes
        return response.json()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))
