from pydantic import BaseModel

class SourceCreate(BaseModel):
    host: str
    database: str
    username: str
    password: str
    name : str
    
class DestinationCreate(BaseModel):
    cluster_url: str
    username: str
    password: str
    database: str
    name: str
    
    
class ConnectionCreate(BaseModel):
    name: str
    source_id: str
    destination_id: str

class sync_connection(BaseModel):
    connection_id: str
