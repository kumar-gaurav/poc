from fastapi import FastAPI, HTTPException,APIRouter
import requests
from dotenv import load_dotenv
import os
from . import schema
load_dotenv()

router = APIRouter()
auth_token = os.getenv("AUTH_TOKEN")
workspace_id = os.getenv("WORKSPACE_ID")
sync_connection_url = os.getenv("SYNC_CONNECTION_URL")


@router.post("/start_sync_job/")
async def start_sync_job(sync_data: schema.sync_connection):
    url = sync_connection_url
    headers = {
        "accept": "application/json",
        "content-type": "application/json",
        "authorization": f"Bearer {auth_token}"
    }
    payload = {
        "jobType": "sync", 
        "connectionId": sync_data.connection_id,
    }

    try:
        response = requests.post(url, json=payload, headers=headers)
        response.raise_for_status()  # Raise an HTTPError for non-2xx status codes
        return response.json()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))



@router.get("/job/{job_id}")
async def get_job(job_id: int):
    url = f"{sync_connection_url}/{job_id}"
    headers = {
        "accept": "application/json",
        "authorization": f"Bearer {auth_token}"
    }

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()  # Raise an HTTPError for non-2xx status codes
        return response.json()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))

