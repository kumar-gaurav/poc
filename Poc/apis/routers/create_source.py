from fastapi import FastAPI, HTTPException, APIRouter
import requests
from dotenv import load_dotenv
from . import schema
import os
load_dotenv()


router = APIRouter()
auth_token = os.getenv("AUTH_TOKEN")
workspace_id = os.getenv("WORKSPACE_ID")
create_source_postgres_url = os.getenv("CREATE_SOURCE_URL")



@router.post("/create_source_postgres/")
async def create_source(source_data: schema.SourceCreate):
    url = create_source_postgres_url
    headers = {
        "accept": "application/json",
        "content-type": "application/json",
        "authorization": f"Bearer {auth_token}"
    }
    payload = {
        "configuration": {
            "sourceType": "postgres",
            "port": 5432,
            "ssl_mode": {"mode": "require"},
            "replication_method": {"method": "Xmin"},
            "tunnel_method": {"tunnel_method": "NO_TUNNEL"},
            "host": source_data.host,
            "database": source_data.database,
            "username": source_data.username,
            "password": source_data.password
        },
        "name": source_data.name,
        "workspaceId": workspace_id
    }

    try:
        response = requests.post(url, json=payload, headers=headers)
        response.raise_for_status()  # Raise an HTTPError for non-2xx status codes
        return response.json()
    except requests.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/list_sources/")
async def list_sources():
    url = "https://api.airbyte.com/v1/sources?includeDeleted=false&limit=20&offset=0"
    headers = {
        "accept": "application/json",
        "authorization": f"Bearer {auth_token}"
    }

    response = requests.get(url, headers=headers)

    if response.status_code != 200:
        raise HTTPException(status_code=response.status_code, detail=response.text)

    return response.json()