from fastapi import FastAPI
import uvicorn
from .routers.create_source import router as create_source_router
from .routers.create_destination import router as create_destination_router
from .routers.create_connection import router as create_connection_router
from .routers.sync_connection import router as sync_connection_router


app = FastAPI()
app.include_router(create_source_router)
app.include_router(create_destination_router)
app.include_router(create_connection_router)
app.include_router(sync_connection_router)


