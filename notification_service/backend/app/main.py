from fastapi import FastAPI, HTTPException, Header, Request
from pydantic import BaseModel
import requests
import os
from supabase import create_client, Client
from github import Github
from app.middleware.cors import add_cors_middleware

app = FastAPI()
add_cors_middleware(app)

# Supabase configuration
SUPABASE_URL = os.getenv("SUPABASE_URL")
SUPABASE_KEY = os.getenv("SUPABASE_KEY")
supabase: Client = create_client(SUPABASE_URL, SUPABASE_KEY)


class Codebase(BaseModel):
    token: str
    repo: str


class CreateWebhookRequest(BaseModel):
    codebaseUrl: str
    personalAccessToken: str
    webhookUrl: str


class Webhook(BaseModel):
    webhookUrl: str


@app.get("/")
def health_check():
    return {"message": "NOTIPIPE"}


class SlackMessage(BaseModel):
    text: str


@app.post("/create-webhook-github-slack")
async def create_webhook_github_slack(request: CreateWebhookRequest):
    codebase_url = request.codebaseUrl
    personal_access_token = request.personalAccessToken
    webhook_url = request.webhookUrl

    def extract_github_info(url: str):
        import re

        regex = r"https:\/\/github\.com\/([^\/]+)\/([^\/]+)(?:\.git)?"
        match = re.match(regex, url)
        if match:
            return {"owner": match.group(1), "repo": match.group(2)}
        else:
            return None

    try:
        github = Github(personal_access_token)
        github_info = extract_github_info(codebase_url)
        if not github_info:
            raise HTTPException(status_code=400, detail="Invalid codebase URL")

        owner = github_info["owner"]
        repo = github_info["repo"].split(".")[0]

        repository = github.get_repo(f"{owner}/{repo}")
        config = {"url": webhook_url, "content_type": "json", "insecure_ssl": "0"}
        events = ["push", "pull_request"]

        existing_hooks = repository.get_hooks()
        print(f"____________existing_hooks: {existing_hooks}")
        for hook in existing_hooks:
            if hook.config.get("url") == webhook_url:
                return HTTPException(
                    status_code=409,
                    detail={"message": "Hook already exists.", "hook": hook.raw_data},
                )

        hook = repository.create_hook(
            name="web", config=config, events=events, active=True
        )

        return {"message": "Webhook created successfully", "hook": hook.raw_data}

    except Exception as e:
        print("error:", e)
        raise HTTPException(
            status_code=500, detail="Error occurred while creating webhook"
        )


@app.post("/send-slack-message")
async def send_slack_message(request: Request, x_github_event: str = Header(None)):
    print(f"____________________________________x_github_event: {x_github_event}")
    user_id = "ade2327f-2c80-40ef-8b00-55728726766f"
    slack_webhook_url = "https://hooks.slack.com/services/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"

    if not user_id:
        raise HTTPException(status_code=404, detail="User not found")

    if x_github_event is None:
        raise HTTPException(status_code=400, detail="Missing GitHub event header")

    payload = await request.json()
    print(f"___________________payload: {payload}")

    message = ""

    # Differentiate between events and create appropriate message
    if x_github_event == "push":
        ref = payload.get("ref")
        pusher = payload.get("pusher", {}).get("name")
        repo_name = payload.get("repository", {}).get("name")
        message = f"Push event on {repo_name} by {pusher} to {ref}"
    elif x_github_event == "pull_request":
        action = payload.get("action")
        pr_number = payload.get("number")
        repo_name = payload.get("repository", {}).get("name")
        message = f"Pull request #{pr_number} {action} on {repo_name}"
    elif x_github_event == "ping":
        message = f""
    else:
        message = f"Unhandled event type: {x_github_event}"

    headers = {"Content-Type": "application/json"}

    slack_payload = {"text": message}

    try:
        response = requests.post(slack_webhook_url, headers=headers, json=slack_payload)
        response.raise_for_status()
        return {"message": "Message sent successfully"}
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail="Something went wrong") from e


@app.post("/send-microsoft-teams-message")
async def send_microsoft_teams_message(
    request: Request, x_github_event: str = Header(None)
):
    print(f"____________________________________x_github_event: {x_github_event}")
    user_id = "ade2327f-2c80-40ef-8b00-55728726766f"
    slack_webhook_url = "https://telsagroupsa.webhook.office.com/webhookb2/cae9dd29-6bfc-4687-aad8-5bdfc8aecacd@17f81c98-83cb-48a3-9edd-c7e44cfb8138/IncomingWebhook/9abf7b0e193a43a192da480acba1a837/3a4ea270-d0bc-4cda-89ed-db7135757c17"

    if not user_id:
        raise HTTPException(status_code=404, detail="User not found")

    if x_github_event is None:
        raise HTTPException(status_code=400, detail="Missing GitHub event header")

    payload = await request.json()
    print(f"___________________payload: {payload}")

    message = ""

    # Differentiate between events and create appropriate message
    if x_github_event == "push":
        ref = payload.get("ref")
        pusher = payload.get("pusher", {}).get("name")
        repo_name = payload.get("repository", {}).get("name")
        message = f"Push event on {repo_name} by {pusher} to {ref}"
    elif x_github_event == "pull_request":
        action = payload.get("action")
        pr_number = payload.get("number")
        repo_name = payload.get("repository", {}).get("name")
        message = f"Pull request #{pr_number} {action} on {repo_name}"
    # Add more events as needed
    else:
        message = f"Unhandled event type: {x_github_event}"

    headers = {"Content-Type": "application/json"}

    slack_payload = {"text": message}

    try:
        response = requests.post(slack_webhook_url, headers=headers, json=slack_payload)
        response.raise_for_status()
        return {"message": "Message sent successfully"}
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail="Something went wrong") from e
