import { Metadata, Viewport } from "next";

export type SiteConfig = typeof siteConfig;

export const siteConfig = {
  name: "notipipe",
  description: "notpipe",
  navItems: [
    {
      label: "About",
      href: "/about",
    },
  ],
  navMenuItems: [
    {
      label: "About",
      href: "/about",
    },
  ],
  links: {
    github: "https://github.com/harry-io",
  },
};

export const metadata: Metadata = {
  title: {
    default: siteConfig.name,
    template: `%s - ${siteConfig.name}`,
  },
  description: siteConfig.description,
  icons: {
    icon: "/favicon.ico",
  },
};

export const viewport: Viewport = {
  themeColor: [
    { media: "(prefers-color-scheme: light)", color: "white" },
    { media: "(prefers-color-scheme: dark)", color: "black" },
  ],
};

export const urlConfig = {
  home: "/",
  connect: {
    connect: "/connect",
    github: "/connect/github",
    gitlab: "/connect/gitlab",
    bitbucket: "/connect/bitbucket",
  },
};
