import {
  FaUser,
  FaGithubAlt,
  FaGitlab,
  FaBitbucket,
  FaLinkedinIn,
  FaSlack,
  FaDiscord,
} from "react-icons/fa";
import { FaXTwitter } from "react-icons/fa6";
import { IoNotifications } from "react-icons/io5";
import { BiNotification } from "react-icons/bi";
import { TiChevronRight } from "react-icons/ti";
import { BsMicrosoftTeams } from "react-icons/bs";

export const icons = {
  BellIcon: IoNotifications,
  UserIcon: FaUser,
  LogoIcon: BiNotification,
  GithubIcon: FaGithubAlt,
  GitlabIcon: FaGitlab,
  BitBucketIcon: FaBitbucket,
  ChevronRightIcon: TiChevronRight,
  LinkedinIcon: FaLinkedinIn,
  XTwitterIcon: FaXTwitter,
  SlackIcon: FaSlack,
  MSTeamsIcon: BsMicrosoftTeams,
  DiscordIcon: FaDiscord,
};
