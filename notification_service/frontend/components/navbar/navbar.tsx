"use client";

import { usePathname } from "next/navigation";

import { Navbar as NextUINavbar } from "@nextui-org/navbar";

import { NavbarMenuComp } from "./navbarMenuComp";
import { NavbarComp } from "./navbarComp";

export const Navbar = () => {
  const pathname = usePathname();

  if (pathname !== "/") return;

  return (
    <NextUINavbar maxWidth="xl" position="sticky" shouldHideOnScroll>
      <NavbarComp />
      <NavbarMenuComp />
    </NextUINavbar>
  );
};
