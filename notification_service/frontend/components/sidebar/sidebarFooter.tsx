import { icons } from "@/config/icons";
import { siteConfig } from "@/config/site";
import { Avatar } from "@nextui-org/avatar";
import { Button } from "@nextui-org/button";
import { Link } from "@nextui-org/link";
import { IconWrapper } from "../others/iconWrapper";

export const SidebarFooter = () => {
  const iconsList = [
    {
      title: "github",
      icon: icons.GithubIcon,
      link: "",
    },
    {
      title: "x",
      icon: icons.XTwitterIcon,
      link: "",
    },
    {
      title: "linkedin",
      icon: icons.LinkedinIcon,
      link: "",
    },
  ];
  return (
    <footer className="absolute bottom-2 left-0 w-full flex items-center justify-center py-3">
      <div className="w-11/12 m-auto flex flex-col gap-8 items-center justify-between gap-3">
        {/* <div className="flex items-center justify-between hover:bg-secondary hover:bg-opacity-[10%] w-full p-2 rounded-lg cursor-pointer">
          <Avatar
            showFallback
            isBordered
            color="secondary"
            src="https://i.pravatar.cc/150?u=a042581f4e29026704d"
            size="sm"
          />
          <icons.ChevronRightIcon />
        </div> */}

        <div className="w-full flex items-center justify-center gap-5">
          {iconsList &&
            iconsList.map((el: any) => (
              <Link key={el.title} href={el.link}>
                <IconWrapper className="bg-secondary/10 text-secondary">
                  <el.icon />
                </IconWrapper>
              </Link>
            ))}
        </div>

        <div>
          <Link
            isExternal
            className="flex items-center gap-1 text-current"
            href="https://github.com/harry-io"
            title="github harry-io"
          >
            <span className="text-default-600">Built by</span>
            <p className="text-secondary">{siteConfig.name}</p>
          </Link>
        </div>
      </div>
    </footer>
  );
};
