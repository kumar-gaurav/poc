"use client";

import React from "react";
import { icons } from "@/config/icons";
import { Avatar, Badge, Listbox, ListboxItem } from "@nextui-org/react";
import { ThemeSwitch } from "../theme/theme-switch";
import { urlConfig } from "@/config/site";
import { IconWrapper } from "../others/iconWrapper";
import { usePathname } from "next/navigation";
import { SidebarFooter } from "./sidebarFooter";

export const Sidebar = () => {
  const pathname = usePathname();

  return (
    <div className="h-dvh relative">
      <div className="relative flex h-full w-72 flex-1 flex-col border-r-small border-divider p-6">
        <div className="flex items-center justify-between">
          <div className="flex items-center justify-center hover:text-secondary-600">
            <a href="/" className="flex items-center gap-1 cursor-pointer">
              <icons.LogoIcon />
              <p>notipipe</p>
            </a>
          </div>
          <div className="flex items-center gap-2">
            <ThemeSwitch />
            <Badge content="0" color="secondary" variant="solid">
              <icons.BellIcon style={{ fontSize: "22px" }} />
            </Badge>
          </div>
        </div>
        {/*  */}
        <div className="mt-12">
          <Listbox
            variant="flat"
            aria-label="Connection menu"
            itemClasses={{
              base: "px-3 first:rounded-t-medium last:rounded-b-medium rounded-none gap-3 h-12 data-[hover=true]:bg-default-100/80",
            }}
          >
            <ListboxItem
              endContent={
                <IconWrapper
                  className={
                    pathname.includes(urlConfig.connect.github)
                      ? "text-secondary"
                      : "text-slate-700"
                  }
                >
                  <icons.ChevronRightIcon />
                </IconWrapper>
              }
              href={urlConfig.connect.github}
              key="github"
              startContent={
                <IconWrapper className="bg-secondary/10 text-secondary">
                  <icons.GithubIcon />
                </IconWrapper>
              }
            >
              Github
            </ListboxItem>
            <ListboxItem
              endContent={
                <IconWrapper
                  className={
                    pathname.includes(urlConfig.connect.gitlab)
                      ? "text-warning"
                      : "text-slate-700"
                  }
                >
                  <icons.ChevronRightIcon />
                </IconWrapper>
              }
              href={urlConfig.connect.gitlab}
              key="gitlab"
              startContent={
                <IconWrapper className="bg-warning/10 text-warning">
                  <icons.GitlabIcon />
                </IconWrapper>
              }
            >
              Gitlab
            </ListboxItem>
            <ListboxItem
              endContent={
                <IconWrapper
                  className={
                    pathname.includes(urlConfig.connect.bitbucket)
                      ? "text-primary"
                      : "text-slate-700"
                  }
                >
                  <icons.ChevronRightIcon />
                </IconWrapper>
              }
              href={urlConfig.connect.bitbucket}
              key="bitbucket"
              startContent={
                <IconWrapper className="bg-primary/10 text-primary">
                  <icons.BitBucketIcon />
                </IconWrapper>
              }
            >
              BitBucket
            </ListboxItem>
          </Listbox>
        </div>
        {/*  */}
        <SidebarFooter />
      </div>
    </div>
  );
};
