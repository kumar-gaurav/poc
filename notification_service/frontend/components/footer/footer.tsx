"use client";

import React from "react";
import { Link } from "@nextui-org/link";
import { siteConfig } from "@/config/site";
import { usePathname } from "next/navigation";

export const Footer = () => {
  const pathname = usePathname();

  if (pathname !== "/") return;
  return (
    <div className="absolute bottom-0 left-0 w-full">
      <footer className="w-full flex items-center justify-center py-3">
        <Link
          isExternal
          className="flex items-center gap-1 text-current"
          href="https://github.com/harry-io"
          title="github harry-io"
        >
          <span className="text-default-600">Built by</span>
          <p className="text-secondary">{siteConfig.name}</p>
        </Link>
      </footer>
    </div>
  );
};
