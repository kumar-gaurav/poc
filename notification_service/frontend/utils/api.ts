import axios from "axios";
import { createClient } from "@supabase/supabase-js";

const backendBaseUrl = process.env.NEXT_PUBLIC_API_BASE_URL;
console.log('backendBaseUrl: ', backendBaseUrl);

const supabseUrl = process.env.NEXT_PUBLIC_SUPABASE_URL!;
const supabseAnonKey = process.env.NEXT_PUBLIC_SUPABASE_ANON_KEY!;

export const supabase = createClient(supabseUrl, supabseAnonKey);

export const api = axios.create({
  baseURL: backendBaseUrl,
});
