"use server";

import toast from "react-hot-toast";

import { api, supabase } from "./api";
import { FormData } from "@/app/connect/components/formToConnect";

export const createWebhookGithubMSTeams = async ({
  codebaseUrl,
  personalAccessToken,
  webhookUrl,
}: FormData) => {
  console.log("___________ln__15");
  try {
    const { owner, repo } = extractGitHubInfo(codebaseUrl);
    if (!owner || !repo) {
      toast("Invalid codebase Url.");
      return;
    }
    const res = await api.post("/create-webhook-github-teams", {
      codebaseUrl,
      personalAccessToken,
      webhookUrl,
    });
    console.log("________________res: ", res);
  } catch (error: any) {
    console.log("______________________________________error: ", error);
    // toast.error("Error occured.");
  } finally {
    // setIsLoading(false);
  }
};

export const createWebhookGithubSlack = async ({
  codebaseUrl,
  personalAccessToken,
  webhookUrl,
}: FormData) => {
  console.log("___________ln__42");
  try {
    const { owner, repo } = extractGitHubInfo(codebaseUrl);
    if (!owner || !repo) {
      toast.error("Invalid codebase Url.");
      return;
    }
    const res = await api.post("/create-webhook-github-slack", {
      codebaseUrl,
      personalAccessToken,
      webhookUrl,
    });
    console.log("________________res: ", res);
  } catch (error: any) {
    console.log("error: ", error);
  } finally {
    // setIsLoading(false);
  }
};

export const createWebhookGithubDiscord = async ({
  codebaseUrl,
  personalAccessToken,
  webhookUrl,
}: FormData) => {
  console.log("___________ln__68");
  try {
    const { owner, repo } = extractGitHubInfo(codebaseUrl);
    if (!owner || !repo) {
      toast.error("Invalid codebase Url.");
      return;
    }
    const res = await api.post("/create-webhook-github-discord", {
      codebaseUrl,
      personalAccessToken,
      webhookUrl,
    });
    console.log("________________res: ", res);
  } catch (error: any) {
    console.log("error: ", error);
  } finally {
    // setIsLoading(false);
  }
};

export const handleGoogleLogin = async () => {
  const data = await supabase.auth.signInWithOAuth({
    provider: "google",
    options: {
      redirectTo: `http://localhost:3000`,
    },
  });
};

export const extractGitHubInfo = (
  url: string
): { owner: string; repo: string } | null => {
  const regex = /https:\/\/github\.com\/([^\/]+)\/([^\/]+)(?:\.git)?/;
  const match = url.match(regex);

  if (match && match.length === 3) {
    return {
      owner: match[1],
      repo: match[2].split(".")[0],
    };
  } else {
    return null;
  }
};
