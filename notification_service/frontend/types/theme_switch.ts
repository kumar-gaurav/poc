import { SwitchProps } from "@nextui-org/switch";

export interface ThemeSwitchProps {
  className?: string;
  classNames?: SwitchProps["classNames"];
}
