"use client";

import { HoverBorderGradient } from "@/components/ui/hover-border-gradient";
import Link from "next/link";

export default function Home() {
  return (
    <section className="flex flex-col items-center justify-center gap-4 py-8 md:py-10 h-screen">
      <Link href={"/connect"}>
        <HoverBorderGradient
          containerClassName="rounded-full"
          as="button"
          className="dark:bg-black bg-white text-black dark:text-white flex items-center"
        >
          <span>Connect now</span>
        </HoverBorderGradient>
      </Link>
    </section>
  );
}
