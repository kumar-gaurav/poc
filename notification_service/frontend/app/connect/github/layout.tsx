export default function GithubLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <section className="w-screen">{children}</section>;
}
