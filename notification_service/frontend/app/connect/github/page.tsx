"use client";

import { useState } from "react";
import { FormToConnect } from "../components/formToConnect";
import {
  createWebhookGithubDiscord,
  createWebhookGithubMSTeams,
  createWebhookGithubSlack,
} from "@/utils/fn";
import { Select, SelectItem } from "@nextui-org/react";
import { icons } from "@/config/icons";
import { IconWrapper } from "@/components/others/iconWrapper";
import { IconType } from "react-icons";

interface WebhookPlatforms {
  title: string;
  Icon: IconType;
  key: string;
  className: string;
}
const webhookPlatforms: WebhookPlatforms[] = [
  {
    title: "Teams",
    Icon: icons.MSTeamsIcon,
    key: "teams",
    className: "bg-[#7b83eb]/10 text-[#4b53bc]",
  },
  {
    title: "Slack",
    Icon: icons.SlackIcon,
    key: "slack",
    className: "bg-[#d81c57]/10 text-[#d81c57]",
  },
  {
    title: "Discord",
    Icon: icons.DiscordIcon,
    key: "discord",
    className: "bg-[#5865f2]/10 text-[#5865f2]",
  },
];
export default function Github() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedWebhook, setSelectedWebhook] = useState(webhookPlatforms[0]);

  return (
    <div className="">
      <div className="max-w-md m-auto mb-2 flex items-center justify-end">
        <Select
          className="max-w-[200px]"
          defaultSelectedKeys={["teams"]}
          isOpen={isOpen}
          label="Connect to"
          onOpenChange={(open) => open !== isOpen && setIsOpen(open)}
          placeholder="Webhook"
          variant="bordered"
          startContent={
            <IconWrapper className={`${selectedWebhook.className} bg-[none]`}>
              <selectedWebhook.Icon />
            </IconWrapper>
          }
          onChange={(e) =>
            setSelectedWebhook(
              webhookPlatforms.filter(
                (webhook) => webhook.key === e.target.value
              )[0]
            )
          }
        >
          {webhookPlatforms &&
            webhookPlatforms.map(({ title, key, Icon, className }) => (
              <SelectItem
                startContent={
                  <IconWrapper className={className}>
                    <Icon />
                  </IconWrapper>
                }
                key={key}
              >
                {title}
              </SelectItem>
            ))}
        </Select>
      </div>
      {/*  */}
      <FormToConnect
        trigger={
          selectedWebhook?.key === "teams"
            ? createWebhookGithubMSTeams
            : selectedWebhook?.key === "discord"
              ? createWebhookGithubDiscord
              : createWebhookGithubSlack
        }
      />
    </div>
  );
}
