export default function BitBucketLayout({
    children,
  }: {
    children: React.ReactNode;
  }) {
    return (
      <section className="w-full">
        {children}
      </section>
    );
  }
  