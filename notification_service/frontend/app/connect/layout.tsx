import { Sidebar } from "@/components/sidebar/sidebar";

export default function ConnectLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <section className="flex items-center dark h-screen w-screen">
      <Sidebar />
      {children}
    </section>
  );
}
