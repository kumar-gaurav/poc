"use client";

import { Button } from "@nextui-org/button";
import { Input, Textarea } from "@nextui-org/input";
import { ChangeEvent, FormEvent, useState } from "react";
import { FaEye, FaEyeSlash } from "react-icons/fa";

export interface FormData {
  codebaseUrl: string;
  personalAccessToken: string;
  webhookUrl: string;
}
const initialFormData = {
  codebaseUrl: "",
  personalAccessToken: "",
  webhookUrl: "",
};
export const FormToConnect = ({
  trigger,
}: {
  trigger: (formadata: FormData) => Promise<void>;
}) => {
  const [formData, setFormData] = useState<FormData>(initialFormData);
  const [isVisible, setIsVisible] = useState(false);
  const [loading, setIsLoading] = useState<boolean>(false);

  const toggleVisibility = () => setIsVisible(!isVisible);

  const handleFormDataChange = (value: string, key: keyof FormData) => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      [key]: value,
    }));
  };

  const handleFormSubmit = async (event: FormEvent) => {
    console.log("____submitting....");
    event.preventDefault();
    await trigger(formData);
  };
  return (
    <div className="flex items-center gap-3 text-center justify-center w-full">
      <form
        action="submit"
        className="flex flex-col gap-3 w-full max-w-md"
        onSubmit={handleFormSubmit}
      >
        <Textarea
          classNames={{
            base: "max-w-lg",
            input: "resize-y min-h-[40px]",
          }}
          disableAutosize
          label="Codebase URL"
          onChange={(event: ChangeEvent<HTMLInputElement>) => {
            handleFormDataChange(event.target.value, "codebaseUrl");
          }}
          placeholder="Enter your codebase URL"
          type="text"
          value={formData.codebaseUrl}
          variant="bordered"
        />
        <Input
          className="max-w-lg"
          endContent={
            <button
              className="focus:outline-none"
              type="button"
              onClick={toggleVisibility}
              aria-label="toggle password visibility"
            >
              {isVisible ? (
                <FaEyeSlash className="text-lg text-default-400 pointer-events-none" />
              ) : (
                <FaEye className="text-lg text-default-400 pointer-events-none" />
              )}
            </button>
          }
          label="Personal Access Token"
          onChange={(event: ChangeEvent<HTMLInputElement>) => {
            handleFormDataChange(event.target.value, "personalAccessToken");
          }}
          placeholder="Enter your personal access token"
          type={isVisible ? "text" : "password"}
          value={formData.personalAccessToken}
          variant="bordered"
        />
        <Textarea
          classNames={{
            base: "max-w-lg",
            input: "resize-y min-h-[40px]",
          }}
          disableAutosize
          label="Webhook URL"
          onChange={(event: ChangeEvent<HTMLInputElement>) => {
            handleFormDataChange(event.target.value, "webhookUrl");
          }}
          placeholder="Enter your webhook URL"
          type="text"
          value={formData.webhookUrl}
          variant="bordered"
        />
        <div className="w-full flex items-center max-w-sm">
          <Button
            type="submit"
            color="secondary"
            variant="shadow"
            className="p-6 rounded-full w-5/12"
          >
            Connect
          </Button>
        </div>
      </form>
    </div>
  );
};
