import { NextResponse } from "next/server";

export async function POST() {
  const user_id = "ade2327f-2c80-40ef-8b00-55728726766f";
  const slack_webhook_url =
    "https://hooks.slack.com/services/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";

  if (!user_id) {
    return NextResponse.json({ message: "User not found" });
  }

  const myHeaders = new Headers();
  myHeaders.append("Content-type", "application/json");

  const raw = JSON.stringify({
    text: "Test msg!",
  });

  const requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: undefined,
  };

  try {
    const response = await fetch(slack_webhook_url, requestOptions);
    console.log("response: ", response);
    return new NextResponse();
  } catch (error) {
    return Response.json(
      {
        message: "Something went wrong",
      },
      {
        status: 500,
      }
    );
  }
}
