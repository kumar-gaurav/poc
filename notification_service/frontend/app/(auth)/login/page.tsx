"use client";

import { Suspense } from "react";
import { supabase } from "@/utils/api";
import { handleGoogleLogin } from "@/utils/fn";
console.log("supabase: ", supabase);

const Login = (): JSX.Element => {
  return (
    <Suspense fallback="Loading...">
      <div className="w-full h-screen">
        <main className="h-full flex flex-col items-center justify-center">
          <section className="w-full">
            <button onClick={handleGoogleLogin}>Sign in with google</button>
          </section>
        </main>
      </div>
    </Suspense>
  );
};

export default Login;
