# NOTICATION SERVICE
An app for connecting codebases and workspaces easily for receiving notifications.

## Setup Guide

### Backend Setup

To build and start the backend:

```bash
docker-compose up --build
```

### Frontend Setup

```bash
cd frontend
npm i
npm run dev
```