# Airbyte and Telsa Gain MVP Setup

### Clone Airbyte from GitHub
```git clone --depth=1 https://github.com/airbytehq/airbyte.git```

### Switch into Airbyte directory
```cd airbyte```

### Start Airbyte
```./run-ab-platform.sh```

### Switch into the Telsa Gain MVP directory
```cd Telsa_gain_mvp```

### Activate the virtual environment
```python3 -m venv venv```
```source venv/bin/activate```

###  Install the required packages
```pip install -r requirements.txt```

### Start the server
```uvicorn app.main:app --port 5000 --reload```
