base_url = "http://localhost:8000/api"



# for workspaces
creating_workspace = "/v1/workspaces/create"
update_workspace_name = "/v1/workspaces/update_name"
deleting_workspace = "/v1/workspaces/delete"
getting_all_workspaces = "/v1/workspaces/list"
getting_workspace_by_id = "/v1/workspaces/get"



# for source definitions
getting_all_source_definitions = "/v1/source_definitions/list"
getting_source_definition_by_id = "/v1/source_definitions/get"
source_definition_specifications_fields = "/v1/source_definition_specifications/get"



# for destination definitions
getting_all_destination_definitions = "/v1/destination_definitions/list"
getting_destination_definition_by_id = "/v1/destination_definitions/get"
destination_definition_specifications_fields = "/v1/destination_definition_specifications/get"



# for source
creating_source = "/v1/sources/create"
source_check_connection = "/v1/sources/check_connection"
deleting_source = "/v1/sources/delete"
getting_all_sources = "/v1/sources/list"
getting_source_by_id = "/v1/sources/get"
updating_source = "/v1/sources/update"



# for destination
creating_destination = "/v1/destinations/create"
destination_check_connection = "/v1/destinations/check_connection"
deleting_destination = "/v1/destinations/delete"
getting_all_destinations = "/v1/destinations/list"
getting_destination_by_id = "/v1/destinations/get"
updating_destination = "/v1/destinations/update"



# for connection
creating_connection = "/v1/connections/create"
deleting_connection = "/v1/connections/delete"
getting_connection_by_id = "/v1/connections/get"
getting_all_connections = "/v1/connections/list"
list_all_connections_including_deleted_ones = "/v1/connections/list_all"
sync_connection = "/v1/connections/sync"



# for jobs
canceling_job = "/v1/jobs/cancel"
getting_job_information = "/v1/jobs/get"
getting_debug_info = "/v1/jobs/get_debug_info"
list_of_jobs = "/v1/jobs/list"
