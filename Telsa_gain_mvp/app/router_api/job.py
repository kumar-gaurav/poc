from fastapi import FastAPI, HTTPException,APIRouter
import requests , os
from app.auth import header_details
from .. import schema
from dotenv import load_dotenv
load_dotenv()


router = APIRouter(tags=["Job"])
base_url = os.getenv("BASE_URL")


@router.post("/api/job/sync")
def sync_connection(request: schema.startConnectionSync):
    url = f"{base_url}/v1/connections/sync"

    try:
        header_info = header_details()
        payload = {
            "connectionId": request.connection_id
        }
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))



@router.post("/api/job/cancel_sync")
def cancel_sync_connection(request: schema.cancelJob):
    url = f"{base_url}/v1/jobs/cancel"

    try:
        header_info = header_details()
        payload = {
            "id": request.job_id
        }
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))
    
    
@router.post("/api/job/info")
def job_info(request: schema.cancelJob):
    url = f"{base_url}/v1/jobs/cancel"

    try:
        header_info = header_details()
        payload = {
            "id": request.job_id
        }
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))
    


@router.post("/api/job/get_debug_info")
def ger_debug_info(request: schema.cancelJob):
    url = f"{base_url}/v1/jobs/get_debug_info"

    try:
        header_info = header_details()
        payload = {
            "id": request.job_id
        }
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))