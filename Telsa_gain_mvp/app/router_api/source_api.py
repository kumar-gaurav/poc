from fastapi import FastAPI, HTTPException,APIRouter
import requests , os
from app.auth import header_details
from .. import schema
from dotenv import load_dotenv
load_dotenv()


router = APIRouter(tags=["Source"])
base_url = os.getenv("BASE_URL")


@router.post("/api/source/create_source")
def create_source(request: schema.createSourceRequest):
    url = f"{base_url}/v1/sources/create"
    payload = {
        "workspaceId": request.workspace_id,
        "sourceDefinitionId": request.sourceDefinitionId,
        "connectionConfiguration": request.configuration,
        "name": request.name
    }
    try:
        header_info = header_details()
        print(header_info)
        response = requests.post(url, json=payload, headers=header_info)
        print(response.text)
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.post("/api/source/all")
def get_source_all(request: schema.getAllSources):
    url = f"{base_url}/v1/sources/list"
    payload = {
        "workspaceId": request.workspace_id
    }
    try:
        header_info = header_details()
        print(header_info)
        response = requests.post(url, json=payload, headers=header_info)
        print(response.text)
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))

@router.post("/api/source/get_schemas")
def get_source_all(request: schema.getSchemas):
    url = f"{base_url}/v1/sources/discover_schema"
    payload = {
        "disable_cache": request.disable_cache,
        "notifySchemaChange": request.notifySchemaChange,
        "sourceId": request.source_id,
        }
    try:
        header_info = header_details()
        response = requests.post(url, json=payload, headers=header_info)

        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))
    
# TESTING API 
@router.post("/job_info")
def get_source_all():
    url = f"{base_url}/v1/jobs/get_debug_info"
    payload = {
        "id": 10
    }
    try:
        header_info = header_details()
        response = requests.post(url, json=payload, headers=header_info)
        
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))