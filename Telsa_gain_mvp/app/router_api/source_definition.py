from fastapi import FastAPI, HTTPException,APIRouter
import requests , os
from app.auth import header_details
from dotenv import load_dotenv
load_dotenv()


router = APIRouter(tags=["Source Definition"])
base_url = os.getenv("BASE_URL")

@router.post("/api/source/list_definitions")
def list_source_definitions():
    url = f"{base_url}/v1/source_definitions/list"
    try:
        header_info = header_details()
        response = requests.post(url, headers=header_info)
        response.raise_for_status()
        return response.json().get('sourceDefinitions', [])
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))