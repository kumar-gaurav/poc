from fastapi import FastAPI, HTTPException,APIRouter
import requests , os
from app.auth import header_details
from .. import schema
from dotenv import load_dotenv
load_dotenv()


router = APIRouter(tags=["Connection"])
base_url = os.getenv("BASE_URL")

@router.post("/api/connection/create_connection")
def create_destination(request: schema.createConnection):
    url = f"{base_url}/v1/connections/create"
    payload = request.data
    try:
        header_info = header_details()
        print(header_info)
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.post("/api/connection/current")
def get_current_connection(request: schema.getAllConnection):
    url = f"{base_url}/v1/connections/list"
    
    try:
        header_info = header_details()
        payload = {
            "workspaceId": request.workspace_id
        }
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))
    

@router.post("/api/connection/all")
def get_deleted_also(request: schema.getAllConnection):
    url = f"{base_url}/v1/connections/list_all"
    
    try:
        header_info = header_details()
        payload = {
            "workspaceId": request.workspace_id
        }
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))

    

@router.post("/api/connection/delete")
def delete_connection(request: schema.deleteConnection):
    url = f"{base_url}/v1/connections/delete"   

    try:
        header_info = header_details()
        payload = {
            "connectionId": request.connection_id
        }
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        if response.content:
            try:
                return response.json()
            except ValueError:
                return {"detail": "Successfully deleted, but response is not valid JSON."}
        else:
            return {"detail": "Successfully deleted, but no content returned."}
        
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))

