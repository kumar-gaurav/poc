from fastapi import FastAPI, HTTPException,APIRouter
import requests , os
from app.auth import header_details
from .. import schema
from dotenv import load_dotenv
load_dotenv()


router = APIRouter(tags=["Destination"])
base_url = os.getenv("BASE_URL")

@router.post("/api/destination/create_destination")
def create_destination(request: schema.createDestinationRequest):
    url = f"{base_url}/v1/destinations/create"
    payload = {
        "workspaceId": request.workspace_id,
        "destinationDefinitionId": request.destinationDefinitionId,
        "connectionConfiguration": request.configuration,
        "name": request.name
    }
    try:
        header_info = header_details()
        print(header_info)
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.post("/api/destination/all")
def get_destination_all(request: schema.getAlldestinations):
    url = f"{base_url}/v1/destinations/list"
    payload = {
        "workspaceId": request.workspace_id
    }
    try:
        header_info = header_details()
        print(header_info)
        response = requests.post(url, json=payload, headers=header_info)
        print(response.text)
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))



@router.post("/api/destination/check_connection")
def check_destination_connection(request: schema.checkConnectionDestination):
    url = f"{base_url}/v1/destinations/check_connection"
    payload = {
        "destinationId": request.destination_id
    }
    try:
        header_info = header_details()
        print(header_info)
        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))
