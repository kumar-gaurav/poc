from fastapi import FastAPI, HTTPException,APIRouter
import requests , os
from app.auth import header_details
from .. import schema
from dotenv import load_dotenv
load_dotenv()


router = APIRouter(tags=["Workspace"])
base_url = os.getenv("BASE_URL")


@router.post("/api/workspace/create")
def create_workspace(request: schema.createWorkspace):
    url = f"{base_url}/v1/workspaces/create"

    try:
        header_info = header_details()
        payload = {
        "email": request.email,
        "name": request.name,
        "organizationId": request.organization_id
        }

        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.post("/api/workspace/all")
def get_all_workspaces():
    url = f"{base_url}/v1/workspaces/list"

    try:
        header_info = header_details()
        response = requests.post(url, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))
    

@router.post("/api/workspace/current")
def delete_workspace(request: schema.singleWorkspace):
    url = f"{base_url}/v1/workspaces/get"

    try:
        header_info = header_details()
        payload = {
            "workspaceId": request.workspace_id
        }

        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))



@router.post("/api/workspace/delete")
def delete_workspace(request: schema.singleWorkspace):
    url = f"{base_url}/v1/workspaces/delete"

    try:
        header_info = header_details()
        payload = {
            "workspaceId": request.workspace_id
        }

        response = requests.post(url, json=payload, headers=header_info)
        response.raise_for_status()
        if response.content:
            try:
                return response.json()
            except ValueError:
                return {"detail": "Successfully deleted, but response is not valid JSON."}
        else:
            return {"detail": "Successfully deleted, but no content returned."}

    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=str(e))

