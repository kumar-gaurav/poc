from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from .router_api import destination_api, destination_definition, source_api, source_definition, connection, job, workspace


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"], 
    allow_credentials=True,
    allow_methods=["*"], 
    allow_headers=["*"],  
)

app.include_router(destination_api.router)
app.include_router(destination_definition.router)
app.include_router(source_api.router)
app.include_router(source_definition.router)
app.include_router(connection.router)
app.include_router(job.router)
app.include_router(workspace.router)

@app.get("/")
def home_route():   
    return "Fastapi server running..."
