from pydantic import BaseModel


class createDestinationRequest(BaseModel):
    configuration: dict
    workspace_id: str
    name: str
    destinationDefinitionId: str

class createSourceRequest(BaseModel):
    configuration: dict
    workspace_id: str
    name: str
    sourceDefinitionId: str

class getAllSources(BaseModel):
    workspace_id: str

class getAlldestinations(BaseModel):
    workspace_id: str

class createConnection(BaseModel):
    data: dict

class getAllConnection(BaseModel):
    workspace_id: str

class startConnectionSync(BaseModel):
    connection_id: str

class deleteConnection(BaseModel):
    connection_id: str

class checkConnectionDestination(BaseModel):
    destination_id: str

class getSchemas(BaseModel):
    disable_cache: bool
    notifySchemaChange: bool
    source_id: str

class cancelJob(BaseModel):
    job_id: int


class createWorkspace(BaseModel):
    email: str
    name: str
    organization_id: str

class singleWorkspace(BaseModel):
    workspace_id : str