import base64
from dotenv import load_dotenv
import os
load_dotenv()


def header_details():

    credentials = f"{os.getenv('AUTH_USERNAME')}:{os.getenv('AUTH_PASSWORD')}"
    # Base64 encode the credentials
    encoded_credentials = base64.b64encode(credentials.encode()).decode()

    headers = {
        "accept": "application/json",
        "content-type": "application/json",
        "Authorization": f"Basic {encoded_credentials}"
    }
    
    return headers